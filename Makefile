# =========================================================================== #
# Configuration 
PREFIX=/usr/local
CGIDIR=/home/httpd/cgi-bin

# =========================================================================== #
# Do not edit the following unless you know what are you are doing

# Html2Wml version
VERSION=0.4.11

# Standard subdirectories locations
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/man
DOCDIR=$(PREFIX)/doc/html2wml

MAN1DIR=$(MANDIR)/man1

# Default target
default: check-config
	@echo 
	@echo "Current configuration: "
	@echo     PREFIX=$(PREFIX)
	@echo "     => BINDIR=$(BINDIR)"
	@echo "        MANDIR=$(MANDIR)"
	@echo "        DOCDIR=$(DOCDIR)"
	@echo "    CGIDIR=$(CGIDIR)"
	@echo 
	@echo "Type \"make install\" to install Html2Wml."
	@echo "Type \"make installcgi\" to install Html2Wml in your CGI directory."

# Installaton targets
install: check-config directories
	install -m 555 html2wml.cgi $(BINDIR)/html2wml
	install -m 444 doc/html2wml.man $(MAN1DIR)/html2wml.1 
	install -m 444 COPYING INSTALL NEWS NOTES README TODO ChangeLog $(DOCDIR) 
	make -C t/ install DOCDIR=$(DOCDIR) 
	make -C doc/ install DOCDIR=$(DOCDIR)

installcgi: check-config
	install -m 555 html2wml.cgi $(CGIDIR)

uninstall: 
	rm -f $(BINDIR)/html2wml
	rm -f $(MAN1DIR)/html2wml.1
	rm -rf $(DOCDIR)

directories: 
	install -d $(BINDIR) $(MAN1DIR) $(DOCDIR)

check-config:
	@echo "Checking if your config looks good..."
	@perl checkconfig.pl
	@echo "Sounds ok."

